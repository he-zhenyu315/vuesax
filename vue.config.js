const webpack = require('webpack')
module.exports = {
    configureWebpack: {
        plugins: [
          new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "windows.jQuery": "jquery"
          })
        ]
      },
    // 基本路径
    publicPath: '/',
    // 输出文件目录
    outputDir: 'dist',

    lintOnSave: false,

    runtimeCompiler: false,

    chainWebpack: () => { },
    configureWebpack: () => { },

    productionSourceMap: false,

    parallel: require('os').cpus().length > 1,
    pwa: {},
    // webpack-dev-server 相关配置
    devServer: {
        // open: process.platform === 'darwin',
        //将服务启动后默认打开浏览器
        open: true,
        host: 'localhost',
        port: 4315,
        https: false,
        hotOnly: false,
        proxy: {// 设置代理
            "/xinwen1": {
               
                target: "http://c.m.163.com/nc/article/headline/T1348647853363/0-40.html",
                changeOrigin: true,
                ws: true, //websocket支持
                secure: false,
                pathRewrite: {
                    "^/xinwen1": ""
                }
            },
            "/api": {
                target: "http://124.222.124.71:4000",
                // target: "http://127.0.0.1:4000",
                changeOrigin: true,
                ws: true, //websocket支持
                secure: false,
                pathRewrite: {
                    "^/api": ""
                },
               
            }
        
        
        },
        before: app => { }
    },
    // 第三方插件配置
    pluginOptions: {
        // ...
    }
}