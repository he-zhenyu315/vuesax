import Vue from 'vue'
import './assets/fonts/font.css'
import App from './App.vue'
import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css'
import '@/assets/css/global.css'
import less from 'less'
import 'animate.css';
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.config.productionTip = false
Vue.use(Vuesax)
Vue.use(less)
Vue.use(ElementUI);

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
