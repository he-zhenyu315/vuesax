import request from '@/assets/js/axios'

// 获取所有留言
export function getAllfeedback (params) {
    return request({
        url: '/api/getAllfeedback',
        method: 'get',
        params,
    })
}

// 添加留言
export function addFeedback (params) {
    return request({
        url: '/api/addFeedback',
        method: 'get',
        params,
    })
}

// 获取博客
export function getAllblog (params) {
    return request({
        url: '/api/getAllblog',
        method: 'get',
        params,
    })
}
export function getAllfrontblog2 (params) {
    return request({
        url: '/api/getAllfrontblog2',
        method: 'get',
        params,
    })
}
export function getAllendblog2 (params) {
    return request({
        url: '/api/getAllendblog2',
        method: 'get',
        params,
    })
}
export function getAlldatabaseblog2 (params) {
    return request({
        url: '/api/getAlldatabaseblog2',
        method: 'get',
        params,
    })
}
// 获取博客详情
export function getBlogById (params) {
    return request({
        url: '/api/getBlogById',
        method: 'get',
        params,
    })
}
// 获取新闻
export function getXinwen (params) {
    return request({
        url: '/xinwen1',
        method: 'get',
        params,
    })
}