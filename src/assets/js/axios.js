// 导入axios
import axios from 'axios'
import qs from "qs";
//1. 创建新的axios实例，
const service = axios.create({
    // 公共接口
    // 超时时间 单位是ms，这里设置了5s的超时时间
    timeout: 30 * 1000
})

service.interceptors.request.use(config => {

    config.headers['Content-Type'] = 'application/json'
    //为了判断是否为formdata格式，增加了一个变量为type,如果type存在，而且是form的话，则代表是formData的格式
    if (config.type && config.type === 'form') {
        config.headers['Content-Type'] = 'application/x-www-form-urlencoded'
        //data是接收的数据，接收的数据需要通过qs编码才可以直接使用
        if (config.data) {
            config.data = qs.stringify(config.data)
        }
    }
    return config
}, error => {
    Promise.reject(error)
})

//4.导入文件
export default service
