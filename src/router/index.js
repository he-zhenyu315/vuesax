import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home.vue'
//vue 存在路由重复跳转，会报错的问题:
//加上以下代码，就能防止重复跳转
Vue.use(VueRouter)
const originalPushs = VueRouter.prototype.replace
VueRouter.prototype.replace = function replace (location) {
    return originalPushs.call(this, location).catch(err => err)
}
const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        redirect: "/blog",
        children: [
            {
                path: '/blog',
                name: 'blog',
                component: () => import(/* webpackChunkName: "blog" */ '../views/index.vue')
            },
            {
                path: '/message',
                name: 'message',
                component: () => import(/* webpackChunkName: "message" */ '../views/toMe.vue')
            },
            
            {
                path: '/articleDetail',
                name: 'articleDetail',
                component: () => import(/* webpackChunkName: "articleDetail" */ '../views/articleDetail.vue')
            },
            {
                path: '/blogList',
                name: 'blogList',
                component: () => import(/* webpackChunkName: "articleDetail" */ '../components/blogList.vue')
            },
            {
                path: '/showBloglist',
                name: 'showBloglist',
                component: () => import(/* webpackChunkName: "articleDetail" */ '../components/showBloglist.vue')
            },
            {
                path: '/blogList3',
                name: 'blogList3',
                component: () => import(/* webpackChunkName: "articleDetail" */ '../components/blogList3.vue')
            },
            {
                path: '/frontBlog',
                name: 'frontBlog',
                component: () => import(/* webpackChunkName: "articleDetail" */ '../views/frontBlog.vue')
            },
            {
                path: '/cardBotm',
                name: 'cardBotm',
                component: () => import(/* webpackChunkName: "articleDetail" */ '../components/cardBotm.vue')
            },
            {
                path: '/endBlog',
                name: 'endBlog',
                component: () => import(/* webpackChunkName: "articleDetail" */ '../views/endBlog.vue')
            },
            {
                path: '/databaseBlog',
                name: 'databaseBlog',
                component: () => import(/* webpackChunkName: "articleDetail" */ '../views/databaseBlog.vue')
            },
            {
                path: '/H5C3',
                name: 'H5C3',
                component: () => import(/* webpackChunkName: "articleDetail" */ '../views/H5C3.vue'),
                children:[
                    
                 {path:'/',component: () => import(/* webpackChunkName: "articleDetail" */ '../views/H5C3/Ghost.vue')},
                 {path:'Ghost',component: () => import(/* webpackChunkName: "articleDetail" */ '../views/H5C3/Ghost.vue')},
                 {path:'Ghost2',component: () => import(/* webpackChunkName: "articleDetail" */ '../views/H5C3/Ghost2.vue')},
                 {path:'Ghost3',component: () => import(/* webpackChunkName: "articleDetail" */ '../views/H5C3/Ghost3.vue')},
                 {path:'Ghost4',component: () => import(/* webpackChunkName: "articleDetail" */ '../views/H5C3/Ghost4.vue')},
                 {path:'Ghost5',component: () => import(/* webpackChunkName: "articleDetail" */ '../views/H5C3/Ghost5.vue')},
                
                    
                ]
                   
               
            },
            {
                path: '/Game',
                name: 'Game',
                component: () => import(/* webpackChunkName: "articleDetail" */ '../views/Game.vue'),
                children:[
                    
                    {path:'/',component: () => import(/* webpackChunkName: "articleDetail" */ '../views/JSGame/Game.vue')},
                    {path:'Game',component: () => import(/* webpackChunkName: "articleDetail" */ '../views/JSGame/Game.vue')},
                    {path:'Game2',component: () => import(/* webpackChunkName: "articleDetail" */ '../views/JSGame/Game2.vue')},
                    {path:'Game3',component: () => import(/* webpackChunkName: "articleDetail" */ '../views/JSGame/Game3.vue')},
                   
                       
                   ]
                      
            },
            {
                path: '/xinWen',
                name: 'xinWen',
                component: () => import(/* webpackChunkName: "articleDetail" */ '../views/xinWen.vue')
            },
            {path:'Ghost',component: () => import(/* webpackChunkName: "articleDetail" */ '../views/H5C3/Ghost.vue')},
            {
                path: '/ introDuction',
                name: ' introDuction',
                component: () => import(/* webpackChunkName: "articleDetail" */ '../components/introDuction.vue')
            },
           
        ]
    }
]

const router = new VueRouter({
    mode: 'hash',
    base: process.env.BASE_URL,
    routes
})

export default router
